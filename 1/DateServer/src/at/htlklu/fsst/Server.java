package at.htlklu.fsst;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;

public class Server extends Thread {
	public static final int PORT = 4545;
	private Socket s;
	private BufferedReader in;
	private PrintWriter out;
	private boolean keepRunning = true;

	public static void main(String[] args) {
		try (ServerSocket serverSocket = new ServerSocket(PORT);) {
			while (true) {
				Server server = new Server(serverSocket.accept());
				server.start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Server(Socket s) {
		this.s = s;
		try {
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new PrintWriter(s.getOutputStream(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String parseLine(String line) {
		if (line == null)
			return "Invalid";

		switch (line) {
		case "time":
			return (Helper.getTime(LocalDateTime.now()));
		case "date":
			return (Helper.getDate(LocalDateTime.now()));
		case "exit":
			close();
			return "Bye...";
		default:
			return "Unknown Command!";
		}
	}

	public void close() {
		this.keepRunning = false;
	}

	@Override
	public void run() {
		System.out.println("Running...");
		out.println(Helper.getWelcomeMsg(LocalDateTime.now(), s.getInetAddress().getHostAddress()));

		while (keepRunning) {
			try {
				out.println(parseLine(in.readLine()));
			} catch (IOException e) {
				keepRunning = false;
				e.printStackTrace();
			}
		}

		try {
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
