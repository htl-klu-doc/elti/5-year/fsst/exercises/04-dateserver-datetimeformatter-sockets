package at.htlklu.fsst;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Helper {
	private static String getOrdinal(final int day) {
		if (day >= 11 && day <= 13) {
			return "th";
		}
		return switch (day % 10) {
		case 1 -> "st";
		case 2 -> "nd";
		case 3 -> "rd";
		default -> "th";
		};
	}

	private static String getRawTime(LocalDateTime ldt) {
		DateTimeFormatter time = DateTimeFormatter.ofPattern("k:mma", Locale.UK);
		return time.format(ldt);
	}

	public static String getTime(LocalDateTime ldt) {
		return String.format("It is %s", getRawTime(ldt));
	}

	private static String getRawDate(LocalDateTime ldt) {
		DateTimeFormatter dateMonthDay = DateTimeFormatter.ofPattern("MMMM, d", Locale.ENGLISH);
		DateTimeFormatter dateYear = DateTimeFormatter.ofPattern("yyyy", Locale.ENGLISH);
		return String.format("%s%s, %s", dateMonthDay.format(ldt), getOrdinal(ldt.getDayOfMonth()),
				dateYear.format(ldt));
	}

	public static String getDate(LocalDateTime ldt) {
		return String.format("It is %s", getRawDate(ldt));
	}

	private static String getWelcomeDate(LocalDateTime ldt) {
		return String.format("%s on %s", getRawTime(ldt), getRawDate(ldt));
	}

	public static String getWelcomeMsg(LocalDateTime ldt, String ip) {
		String date = getWelcomeDate(ldt);

		return String.format("Hello %s, it is %s.", ip, date);
	}
}
