package at.htlklu.fsst;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.event.ActionEvent;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextArea;
import java.awt.Dimension;
import javax.swing.JTextField;

public class Client extends JFrame {
	private static final long serialVersionUID = 2297449611745457213L;
	public static final int PORT = 4545;
	private Socket s;
	private BufferedReader in;
	private PrintWriter out;

	private JPanel contentPane;
	private JTextArea tarOut;
	private JTextField txtIp;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Client frame = new Client();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Client() {
		setMinimumSize(new Dimension(203, 297));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 230, 331);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JButton btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String ip = txtIp.getText();
					if (s == null && ip != null) {
						System.out.println(ip);
						s = new Socket(ip, PORT);
						in = new BufferedReader(new InputStreamReader(s.getInputStream()));
						out = new PrintWriter(s.getOutputStream(), true);
						tarOut.setText(in.readLine());
						btnConnect.setText("Disconnect");
					} else {
						out.println("exit");
						s.close();
						s = null;
						btnConnect.setText("Connect");
					}
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});

		JButton btnDate = new JButton("Get Date");
		btnDate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (s != null && s.isConnected()) {
					out.println("date");
					try {
						tarOut.setText(in.readLine());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		JButton btnTime = new JButton("Get Time");
		btnTime.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (s != null && s.isConnected()) {
					out.println("time");
					try {
						tarOut.setText(in.readLine());
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});

		tarOut = new JTextArea();
		tarOut.setLineWrap(true);
		tarOut.setEditable(false);
		
		txtIp = new JTextField();
		txtIp.setText("127.0.0.1");
		txtIp.setColumns(10);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(txtIp, GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(tarOut, GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(15)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(btnConnect, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(btnDate)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnTime, GroupLayout.DEFAULT_SIZE, 98, Short.MAX_VALUE)))))
					.addGap(6))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(tarOut, GroupLayout.PREFERRED_SIZE, 167, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtIp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnDate)
						.addComponent(btnTime))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnConnect)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}
}
